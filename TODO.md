## TODO

-   [x] Graceful shutdown
-   [x] `keep-alive` timeout setting
-   [x] HTTP Etag: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag#etag_value
-   [x] Content compress
    -   [x] zstd
    -   [x] gzip
    -   [x] deflate
    -   [x] br

### Configuration

-   [x] File MIME type
-   [x] Overrite headers in config
-   [x] Config init tests
-   [ ] Logging to file
-   [ ] Docker build file
-   [ ] Benchs
-   [ ] Max body size
-   [ ] Error page

### Features

-   [ ] Proxy
-   [ ] FastCGI
-   [ ] SSL
-   [ ] Cli
    -   [x] Specific custom config location
-   [ ] HTTP 2
-   [ ] HTTP 3
