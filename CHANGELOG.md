## 0.1.0

Features:

- Graceful shutdown
- `keep-alive` timeout setting
- HTTP Etag: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag#etag_value
- Content compress
    - zstd
    - gzip
    - deflate
    - br
- Stream file
- Stream content compress
