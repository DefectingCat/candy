use std::{
    error::Error as StdError,
    io::ErrorKind::{NotConnected, NotFound},
    net::SocketAddr,
    path::Path,
    pin::pin,
    time::{self, Duration, Instant},
};

use crate::{
    config::SettingHost,
    consts::{NAME, VERSION},
    error::{Error, Result},
    http::{handle_get, handle_not_found, internal_server_error, not_found, CandyBody},
    utils::{find_route, parse_assets_path},
};

use anyhow::anyhow;
use futures_util::Future;
use http::{Method, Request, Response};
use hyper::{
    body::{Bytes, Incoming},
    server::conn::http1,
    service::service_fn,
};
use hyper_util::rt::TokioIo;
use tokio::{
    net::{TcpListener, TcpStream},
    select,
};

use tracing::{debug, error, info, warn};

impl SettingHost {
    pub fn mk_server(&'static self) -> impl Future<Output = anyhow::Result<()>> + 'static {
        let addr = format!("{}:{}", self.ip, self.port);
        async move {
            let listener = TcpListener::bind(&addr).await?;
            info!("host bind on {}", addr);
            loop {
                let socket = listener.accept().await?;
                tokio::spawn(async move {
                    graceful_shutdown(self, socket).await;
                });
            }
        }
    }
}

/// Handle tokio TcpListener socket,
/// then use hyper and service_fn to handle connection
///
/// ## Arguments
///
/// `host`: host configuration from config file
/// `socket`: the socket what tokio TcpListener accepted
pub async fn graceful_shutdown(host: &'static SettingHost, socket: (TcpStream, SocketAddr)) {
    let (stream, addr) = socket;
    let io = TokioIo::new(stream);

    // Use keep_alive in config for incoming connections to the server.
    // use process_timeout in config for processing the final request and graceful shutdown.
    let connection_timeout = Duration::from_secs(host.keep_alive.into());

    // service_fn
    let service = move |req: Request<Incoming>| async move {
        let start_time = time::Instant::now();
        let res = handle_connection(&req, host).await;
        let response = match res {
            Ok(res) => res,
            Err(Error::NotFound(err)) => {
                warn!("{err}");
                not_found()
            }
            _ => internal_server_error(),
        };
        let end_time = (Instant::now() - start_time).as_micros() as f32;
        let end_time = end_time / 1000_f32;
        let method = &req.method();
        let path = &req.uri().path();
        let version = &req.version();
        let res_status = response.status();
        info!(
            "\"{}\" {} {} {:?} {} {:.3}ms",
            addr, method, path, version, res_status, end_time
        );
        anyhow::Ok(response)
    };

    let conn = http1::Builder::new().serve_connection(io, service_fn(service));
    let mut conn = pin!(conn);
    // Iterate the timeouts.  Use tokio::select! to wait on the
    // result of polling the connection itself,
    // and also on tokio::time::sleep for the current timeout duration.
    select! {
        res = conn.as_mut() => {
            match res {
                Ok(_) => {
                    debug!("close connection");
                }
                Err(err)
                    if err.source().is_some()
                        && err
                            .source()
                            .unwrap()
                            .downcast_ref::<std::io::Error>()
                            .unwrap_or(&std::io::Error::new(NotFound, &Error::Empty))
                            .kind()
                            == NotConnected =>
                {
                    // The client closed connection
                    debug!("client closed connection");
                }
                Err(err) => {
                    error!("handle connection {:?}", err);
                }
            }
        }
        _ = tokio::time::sleep(connection_timeout) => {
            debug!("keep-alive timeout {}s, calling conn.graceful_shutdown", host.keep_alive);
            conn.as_mut().graceful_shutdown();
        }
    }
}

/// Connection handler in service_fn
pub async fn handle_connection(
    req: &Request<Incoming>,
    host: &'static SettingHost,
) -> Result<Response<CandyBody<Bytes>>> {
    use Error::*;

    let req_path = req.uri().path();
    let req_method = req.method();

    // find route path
    // TODO: router find optimize
    let (router, assets_path) = find_route(req_path, &host.route_map)?;
    debug!("router = {:?}", router);
    debug!("assets_path = {}", assets_path);

    // build the response for client
    let mut res = Response::builder();
    let headers = res
        .headers_mut()
        .ok_or(InternalServerError(anyhow!("build response failed")))?;
    let server = format!("{}/{}", NAME, VERSION);
    headers.insert("Server", server.parse()?);
    // config headers overrite
    if let Some(c_headers) = &host.headers {
        for (k, v) in c_headers {
            headers.insert(k.as_str(), v.parse()?);
        }
    }

    // find resource local file path
    let mut path = None;
    for index in router.index.iter() {
        let p = parse_assets_path(assets_path, &router.root, index);
        if Path::new(&p).exists() {
            path = Some(p);
            break;
        }
    }
    let path = match path {
        Some(p) => p,
        None => {
            return handle_not_found(req, res, router, assets_path).await;
            // return Err(not_found_err);
        }
    };

    // http method handle
    let res = match *req_method {
        Method::GET => handle_get(req, res, &path).await?,
        Method::POST => handle_get(req, res, &path).await?,
        // Return the 404 Not Found for other routes.
        _ => {
            if let Some(err_page) = &router.error_page {
                let res = res.status(err_page.status);
                handle_get(req, res, &err_page.page).await?
            } else {
                not_found()
            }
        }
    };
    Ok(res)
}
